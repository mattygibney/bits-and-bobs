#!/bin/bash

PRIV_FILE=VMWARE15.priv
DER_FILE=VMWARE15.der

echo "Problems with vmmon and vmnet modules, it's normal"
sudo vmware-modconfig --console --install-all

if [ -f "$PRIV_FILE" ]; then
		echo "$PRIV_FILE present, no need to regenerated"
else
		echo "$PRIV_FILE missing, generating a new one"
		openssl req -new -x509 -newkey rsa:2048 -keyout $PRIV_FILE -outform DER -out $DER_FILE -nodes -days 36500 -subj "/CN=VMWARE/"
fi

echo "Now sign the two kernel modules (monitor and net)"
echo "This step is need when kernel is updated"
sudo /usr/src/linux-headers-$(uname -r)/scripts/sign-file sha256 $PRIV_FILE $DER_FILE $(modinfo -n vmmon)
sudo /usr/src/linux-headers-$(uname -r)/scripts/sign-file sha256 $PRIV_FILE $DER_FILE $(modinfo -n vmnet)

echo "Check modules signed"
tail $(modinfo -n vmmon) | grep "Module signature appended"

echo "Test if certs is already enrolled if not we need to import it in mok"
sudo mokutil --test-key VMWARE15.der

#echo "Importing the trusted key in our MOK"
#echo "Enter a bit long password like 1515vmware"
sudo mokutil --import VMWARE15.der
