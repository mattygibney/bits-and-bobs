section .data
    hello db 'Hello, World!\n',0

section .text
    global _start

_start:
    ; write(1, hello, 13)
    mov eax, 4
    mov ebx, 1
    mov ecx, hello
    mov edx, 15
    int 0x80

    ; exit(0)
    mov eax, 1
    xor ebx, ebx
    int 0x80

