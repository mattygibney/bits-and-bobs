section .data
    buffer: times 64 db 0
    fileName db 'example.txt', 0

section .text
    global _start

_start:
    ; open(fileName, 0, 0)
    mov eax, 5
    mov ebx, fileName
    mov ecx, 0
    mov edx, 0
    int 0x80
    mov [fd], eax

    ; read(fd, buffer, 64)
    mov eax, 3
    mov ebx, [fd]
    mov ecx, buffer
    mov edx, 64
    int 0x80

    ; write(1, buffer, 64)
    mov eax, 4
    mov ebx, 1
    mov ecx, buffer
    mov edx, 64
    int 0x80

    ; close(fd)
    mov eax, 6
    mov ebx, [fd]
    int 0x80

    ; exit(0)
    mov eax, 1
    xor ebx, ebx
    int 0x80

