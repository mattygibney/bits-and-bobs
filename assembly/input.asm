section .data
    buffer: times 64 db 0

section .text
    global _start

_start:
    ; read(0, buffer, 64)
    mov eax, 3
    mov ebx, 0
    mov ecx, buffer
    mov edx, 64
    int 0x80

    ; write(1, buffer, 64)
    mov eax, 4
    mov ebx, 1
    mov ecx, buffer
    mov edx, 64
    int 0x80

    ; exit(0)
    mov eax, 1
    xor ebx, ebx
    int 0x80

